package com.datpham.modulo_test

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.datpham.modulo_test.activity.MainActivity
import com.datpham.modulo_test.fragment.profile.ProfileFragment
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock

@RunWith(AndroidJUnit4::class)
@LargeTest
class ProfileFragmentTest {

    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    private lateinit var birthDay: String

    @Before
    fun initValidString() {
        birthDay = "10/10/1995"
        activityRule.scenario.onActivity {
            it.findNavController(R.id.nav_host_fragment).navigate(R.id.profileFragment)
        }
    }

    @Test
    fun changeText_sameActivity() {
        // Type text and then press the button.
        onView(withId(R.id.edtBirthday))
            .perform(typeText(birthDay), closeSoftKeyboard())
        onView(withId(R.id.btnConfirm)).perform(click())
    }

}