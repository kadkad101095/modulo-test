package com.datpham.basekotlin.network.response

import com.datpham.basekotlin.network.entities.Device
import com.datpham.basekotlin.network.entities.User

data class SumUpResponse(val data: ArrayList<Device>)
