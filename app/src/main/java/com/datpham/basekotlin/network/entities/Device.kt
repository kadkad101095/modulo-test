package com.datpham.basekotlin.network.entities

abstract class Device {
    abstract val _id: String
    abstract val name: String
    abstract val productType: String
    abstract var state: String
    abstract val port: String
}
