package com.datpham.basekotlin.network.entities

data class User(
    val firstName: String,
    val lastName: String,
    val address: Address,
    val birthDate: Long,
    val avatar: String? = null
) {
    fun getFullName(): String {
        return "$firstName $lastName"
    }

    fun getDefaultAvatar(): String {
        return avatar ?: "http://learnyzen.com/wp-content/uploads/2017/08/test1-481x385.png"
    }
}
