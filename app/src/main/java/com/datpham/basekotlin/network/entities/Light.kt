package com.datpham.basekotlin.network.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Light(
    @PrimaryKey override val _id: String,
    override val name: String,
    override val productType: String,
    override var state: String,
    override val port: String,
    var intensity: Int,
    var mode: String,
) : Device()
