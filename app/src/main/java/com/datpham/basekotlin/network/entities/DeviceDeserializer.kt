package com.datpham.basekotlin.network.entities

import com.datpham.basekotlin.base.enum.DeviceType
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type


class DeviceDeserializer : JsonDeserializer<Device> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Device {
        val obj = json!!.asJsonObject
        return when (obj.get("productType")?.asString ?: "") {
            DeviceType.ROLLER.type -> context!!.deserialize(obj, RollerShutter::class.java)
            DeviceType.HEATER.type -> context!!.deserialize(obj, Heater::class.java)
            else -> context!!.deserialize(obj, Light::class.java)
        }
    }
}