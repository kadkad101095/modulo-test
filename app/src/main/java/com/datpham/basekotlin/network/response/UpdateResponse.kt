package com.datpham.basekotlin.network.response

import com.datpham.basekotlin.network.entities.Device

data class UpdateResponse(val code: Int, val message: String, val data: Device) {
    fun isSuccess(): Boolean {
        return code == 200
    }
}
