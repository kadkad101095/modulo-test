package com.datpham.basekotlin.network.response

data class ImageResponse(val code: Int, val message: String, val data: ArrayList<String>)