package com.datpham.basekotlin.network.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RollerShutter(
    @PrimaryKey override val _id: String,
    override val name: String,
    override var state: String,
    override val port: String,
    override val productType: String,
    var position: Int
) : Device()
