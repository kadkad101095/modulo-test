package com.datpham.basekotlin.network

import android.annotation.SuppressLint
import android.util.Log
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.utils.BASE_URL
import com.datpham.basekotlin.base.utils.TAG
import com.datpham.basekotlin.network.entities.Device
import com.datpham.basekotlin.network.entities.DeviceDeserializer
import com.datpham.basekotlin.network.exceptions.NetworkConnectionInterceptor
import com.datpham.basekotlin.network.exceptions.NoConnectivityException
import com.datpham.basekotlin.network.response.ImageResponse
import com.datpham.basekotlin.network.response.SumUpResponse
import com.datpham.basekotlin.network.response.UpdateResponse
import com.datpham.modulo_test.R
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@SuppressLint("CheckResult")
class ApiManager {

    private var apiService: ApiService

    init {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(NetworkConnectionInterceptor(App.applicationContext()))
            .build()
        val gson = GsonBuilder()
            .registerTypeAdapter(Device::class.java, DeviceDeserializer())
            .create()

        val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
        apiService = retrofit.create(ApiService::class.java)
    }

    private val errorHandler = Consumer<Throwable?> {
        App.currentActivity().hideLoading()
        Log.e(TAG, it!!.message!!)
        when (it) {
            is NoConnectivityException -> App.currentActivity().showToast(R.string.network_error)
            is HttpException -> {
                when (it.code()) {
                    408 -> App.currentActivity()
                        .showToast(R.string.low_internet_connection) //Time out
                }
            }
        }
    }

    fun getData(success: Consumer<SumUpResponse>) {
        apiService.getData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun updateDevice(device: Device, success: Consumer<UpdateResponse>) {
        apiService.updateDevice(device._id, device.name, device.state, device.port)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun getAllImage(success: Consumer<ImageResponse>) {
        apiService.getAllImage()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

}