package com.datpham.basekotlin.network.entities

data class Address(
    val city: String = "",
    val postalCode: Int = 0,
    val street: String = "",
    val streetCode: String = "",
    val country: String = ""
)
