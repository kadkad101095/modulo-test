package com.datpham.basekotlin.network.entities

/*
* Class nay la entity dung chung trong nhieu truong hop (cast cac object ve dang nay de xu ly chung)
* */
data class CommonData(val id: String = "", val name: String = "", val extra: String = "")