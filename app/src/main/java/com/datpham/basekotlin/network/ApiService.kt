package com.datpham.basekotlin.network

import com.datpham.basekotlin.network.response.ImageResponse
import com.datpham.basekotlin.network.response.SumUpResponse
import com.datpham.basekotlin.network.response.UpdateResponse
import io.reactivex.Observable
import retrofit2.http.*

interface ApiService {

    @GET("/device/getAll")
    fun getData(): Observable<SumUpResponse>

    @FormUrlEncoded
    @POST("/device/update")
    fun updateDevice(
        @Field("id") id: String,
        @Field("name") name: String,
        @Field("state") state: String,
        @Field("port") port: String
    ): Observable<UpdateResponse>

    @GET("/file/getAll")
    fun getAllImage(): Observable<ImageResponse>

}