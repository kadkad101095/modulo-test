package com.datpham.basekotlin.base.utils

import android.util.Patterns
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.round


fun String.isValidDate(format: String): Boolean {
    return try {
        val df: DateFormat = SimpleDateFormat(format, Locale.getDefault())
        df.setLenient(false)
        df.parse(this)
        true
    } catch (e: ParseException) {
        false
    }
}

fun String.isValidEmail(): Boolean {
    if (this == "") return false
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isValidPhone(): Boolean {
    if (this == "" || this.length < 9 || this.length > 11) return false
    return Patterns.PHONE.matcher(this).matches()
}

fun String.toDate(format: String): Date? {
    val sdf = SimpleDateFormat(format, Locale.US)
    return sdf.parse(this)
}

fun Date.toString(format: String): String {
    return SimpleDateFormat(format, Locale.getDefault()).format(this)
}

fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return round(this * multiplier) / multiplier
}