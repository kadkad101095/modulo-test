package com.datpham.basekotlin.base.customviews

import android.view.View
import androidx.databinding.ObservableField
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.context.BaseViewModel
import com.datpham.modulo_test.R

class MyToolbarVM : BaseViewModel() {

    var toolbarTitle = ObservableField(App.getResources().getString(R.string.app_name))
    var isShowLeftButton = ObservableField(true)
    var isShowRightButton = ObservableField(false)

    var leftIcon = ObservableField(R.mipmap.ic_back)
    var rightIcon = ObservableField(R.mipmap.ic_welcome)

    fun onLeftClick(view: View?) {
        if (callbackLeft == null) {
            App.currentActivity().onBackPressed()
        } else {
            callbackLeft?.onToolbarLeftClick()
        }
    }

    fun onRightClick(view: View) {
        callbackRight?.onToolbarRightClick()
    }

    var callbackLeft: CallbackLeft? = null
    var callbackRight: CallbackRight? = null

    fun setOnLeftClick(callback: CallbackLeft) {
        this.callbackLeft = callback
    }

    fun setOnRightClick(callback: CallbackRight) {
        this.isShowRightButton.set(true)
        this.callbackRight = callback
    }

    interface CallbackLeft {
        fun onToolbarLeftClick()
    }

    interface CallbackRight {
        fun onToolbarRightClick()
    }

}