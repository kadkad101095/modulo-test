package com.datpham.basekotlin.base.customviews

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.AttributeSet
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import androidx.core.content.ContextCompat
import com.datpham.modulo_test.R

class MyReadMoreTextView : MyTextView {

    private val TRIM_MODE_LINES = 0
    private val TRIM_MODE_LENGTH = 1
    private val DEFAULT_TRIM_LENGTH = 240
    private val DEFAULT_TRIM_LINES = 2
    private val INVALID_END_INDEX = -1
    private val DEFAULT_SHOW_TRIM_EXPANDED_TEXT = true
    private val ELLIPSIZE = "... "

    private var mText: CharSequence? = null
    private var bufferType: BufferType? = null
    private var readMore = true
    private var trimLength = 0
    private var trimCollapsedText: CharSequence? = null
    private var trimExpandedText: CharSequence? = null
    private var viewMoreSpan: ReadMoreClickableSpan? = null
    private var colorClickableText = 0
    private var showTrimExpandedText = false

    private var trimMode = 0
    private var lineEndIndex = 0
    private var trimLines = 0

    constructor(context: Context) : super(context) {
        MyReadMoreTextView(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyReadMoreTextView)
        trimLength =
            typedArray.getInt(R.styleable.MyReadMoreTextView_trimLength, DEFAULT_TRIM_LENGTH)
        val resourceIdTrimCollapsedText = typedArray.getResourceId(
            R.styleable.MyReadMoreTextView_trimCollapsedText,
            R.string.read_more
        )
        val resourceIdTrimExpandedText = typedArray.getResourceId(
            R.styleable.MyReadMoreTextView_trimExpandedText,
            R.string.read_less
        )
        trimCollapsedText = resources.getString(resourceIdTrimCollapsedText)
        trimExpandedText = resources.getString(resourceIdTrimExpandedText)
        trimLines = typedArray.getInt(R.styleable.MyReadMoreTextView_trimLines, DEFAULT_TRIM_LINES)
        colorClickableText = typedArray.getColor(
            R.styleable.MyReadMoreTextView_colorClickableText,
            ContextCompat.getColor(context, R.color.colorAccent)
        )
        showTrimExpandedText = typedArray.getBoolean(
            R.styleable.MyReadMoreTextView_showTrimExpandedText,
            DEFAULT_SHOW_TRIM_EXPANDED_TEXT
        )
        trimMode = typedArray.getInt(R.styleable.MyReadMoreTextView_trimMode, TRIM_MODE_LINES)
        typedArray.recycle()
        viewMoreSpan = ReadMoreClickableSpan(this)
        onGlobalLayoutLineEndIndex()
        setText()
    }

    private fun setText() {
        super.setText(getDisplayableText(), bufferType)
        movementMethod = LinkMovementMethod.getInstance()
        highlightColor = Color.TRANSPARENT
    }

    private fun getDisplayableText(): CharSequence? {
        return getTrimmedText(mText)
    }

    override fun setText(text: CharSequence?, type: BufferType?) {
        this.mText = text
        bufferType = type
        setText()
    }

    private fun getTrimmedText(text: CharSequence?): CharSequence? {
        if (trimMode == TRIM_MODE_LENGTH) {
            if (text != null && text.length > trimLength) {
                return if (readMore) {
                    updateCollapsedText()
                } else {
                    updateExpandedText()
                }
            }
        }
        if (trimMode == TRIM_MODE_LINES) {
            if (text != null && lineEndIndex > 0) {
                if (readMore) {
                    if (layout.lineCount > trimLines) {
                        return updateCollapsedText()
                    }
                } else {
                    return updateExpandedText()
                }
            }
        }
        return text
    }

    private fun updateCollapsedText(): CharSequence? {
        var trimEndIndex = mText!!.length
        when (trimMode) {
            TRIM_MODE_LINES -> {
                trimEndIndex = lineEndIndex - (ELLIPSIZE.length + trimCollapsedText!!.length + 1)
                if (trimEndIndex < 0) {
                    trimEndIndex = trimLength + 1
                }
            }
            TRIM_MODE_LENGTH -> trimEndIndex = trimLength + 1
        }
        val s = SpannableStringBuilder(mText, 0, trimEndIndex)
            .append(ELLIPSIZE)
            .append(trimCollapsedText)
        return addClickableSpan(s, trimCollapsedText)
    }

    private fun updateExpandedText(): CharSequence? {
        if (showTrimExpandedText) {
            val s = SpannableStringBuilder(mText, 0, mText!!.length).append(trimExpandedText)
            return addClickableSpan(s, trimExpandedText)
        }
        return mText
    }

    private fun addClickableSpan(
        s: SpannableStringBuilder,
        trimText: CharSequence?
    ): CharSequence? {
        s.setSpan(
            viewMoreSpan,
            s.length - trimText!!.length,
            s.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return s
    }

    fun setTrimLength(trimLength: Int) {
        this.trimLength = trimLength
        setText()
    }

    fun setColorClickableText(colorClickableText: Int) {
        this.colorClickableText = colorClickableText
    }

    fun setTrimCollapsedText(trimCollapsedText: CharSequence?) {
        this.trimCollapsedText = trimCollapsedText
    }

    fun setTrimExpandedText(trimExpandedText: CharSequence?) {
        this.trimExpandedText = trimExpandedText
    }

    fun setTrimMode(trimMode: Int) {
        this.trimMode = trimMode
    }

    fun setTrimLines(trimLines: Int) {
        this.trimLines = trimLines
    }

    private class ReadMoreClickableSpan(val tv: MyReadMoreTextView) : ClickableSpan() {

        override fun onClick(widget: View) {
            tv.readMore = !tv.readMore
            tv.setText()
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.color = tv.colorClickableText
        }
    }

    private fun onGlobalLayoutLineEndIndex() {
        if (trimMode == TRIM_MODE_LINES) {
            viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    val obs = viewTreeObserver
                    obs.removeOnGlobalLayoutListener(this)
                    refreshLineEndIndex()
                    setText()
                }
            })
        }
    }

    private fun refreshLineEndIndex() {
        try {
            lineEndIndex = if (trimLines == 0) {
                layout.getLineEnd(0)
            } else if (trimLines > 0 && lineCount >= trimLines) {
                layout.getLineEnd(trimLines - 1)
            } else {
                INVALID_END_INDEX
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
