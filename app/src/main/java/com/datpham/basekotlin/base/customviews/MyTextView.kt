package com.datpham.basekotlin.base.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.datpham.basekotlin.base.context.App.Companion.applicationContext
import com.datpham.modulo_test.R

open class MyTextView : AppCompatTextView {

    var customFont: String? = null

    constructor(context: Context) : super(context) {
        style(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        style(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        style(context, attrs)
    }

    protected fun style(context: Context, attrs: AttributeSet?) {
        if (this.typeface != null) {
            when (this.typeface.style) {
                Typeface.NORMAL -> {
                    val normal = Typeface.createFromAsset(
                        getContext().assets,
                        applicationContext().getString(R.string.font_regular)
                    )
                    setTypeface(normal, Typeface.NORMAL)
                }
                Typeface.BOLD -> {
                    val bold = Typeface.createFromAsset(
                        getContext().assets,
                        applicationContext().getString(R.string.font_bold)
                    )
                    setTypeface(bold, Typeface.BOLD)
                }
                Typeface.ITALIC -> {
                    val italic = Typeface.createFromAsset(
                        getContext().assets,
                        applicationContext().getString(R.string.font_italic)
                    )
                    setTypeface(italic, Typeface.ITALIC)
                }
                Typeface.BOLD_ITALIC -> {
                    val bold_italic = Typeface.createFromAsset(
                        getContext().assets,
                        applicationContext().getString(R.string.font_bold_italic)
                    )
                    setTypeface(bold_italic, Typeface.BOLD_ITALIC)
                }
            }
        } else {
            val normal = Typeface.createFromAsset(
                getContext().assets,
                applicationContext().getString(R.string.font_regular)
            )
            setTypeface(normal, Typeface.NORMAL)
        }
    }
}