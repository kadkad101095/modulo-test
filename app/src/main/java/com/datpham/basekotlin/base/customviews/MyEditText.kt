package com.datpham.basekotlin.base.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.datpham.basekotlin.base.context.App.Companion.applicationContext
import com.datpham.modulo_test.R

class MyEditText : AppCompatEditText {

    private var mContext: Context? = null
    private var attrs: AttributeSet? = null
    private var defStyle = 0

    constructor(context: Context) : super(context) {
        this.mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        this.mContext = context
        this.attrs = attrs
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        this.mContext = context
        this.attrs = attrs
        this.defStyle = defStyle
        init()
    }

    private fun init() {
        if (this.typeface != null) {
            when (this.typeface.style) {
                Typeface.NORMAL -> {
                    val normal = Typeface.createFromAsset(
                        getContext().assets,
                        applicationContext().getString(R.string.font_regular)
                    )
                    setTypeface(normal, Typeface.NORMAL)
                }
                Typeface.BOLD -> {
                    val bold = Typeface.createFromAsset(
                        getContext().assets,
                        applicationContext().getString(R.string.font_bold)
                    )
                    setTypeface(bold, Typeface.BOLD)
                }
                Typeface.ITALIC -> {
                    val italic = Typeface.createFromAsset(
                        getContext().assets,
                        applicationContext().getString(R.string.font_italic)
                    )
                    setTypeface(italic, Typeface.ITALIC)
                }
                Typeface.BOLD_ITALIC -> {
                    val bold_italic = Typeface.createFromAsset(
                        getContext().assets,
                        applicationContext().getString(R.string.font_bold_italic)
                    )
                    setTypeface(bold_italic, Typeface.BOLD_ITALIC)
                }
            }
        } else {
            val normal = Typeface.createFromAsset(
                getContext().assets,
                applicationContext().getString(R.string.font_regular)
            )
            setTypeface(normal, Typeface.NORMAL)
        }
    }
}