package com.datpham.basekotlin.base.utils

const val BASE_URL: String = "http://datlinh.com:7004"

//Cac tham so constant khac
const val TAG: String = "KAD MODULO"
const val DATABASE_NAME = "MODULOTEST"

//HEATER Temperature
const val HEATER_MAX_TEMP = 28
const val HEATER_MIN_TEMP = 7
const val HEATER_STEP = 0.5