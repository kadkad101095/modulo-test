package com.datpham.basekotlin.base.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import com.datpham.basekotlin.base.utils.TAG
import com.datpham.basekotlin.base.customviews.MyTextView
import com.datpham.modulo_test.R
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty1

class BaseSpinnerAdapter<T>(context: Context, resource: Int, list: ArrayList<T>) :
    ArrayAdapter<T>(context, resource, list) {
    var resource: Int
    var listItem: ArrayList<T>
    var layoutInflater: LayoutInflater

    lateinit var nameProp: KProperty1<T, *>
    lateinit var imageProp: KProperty1<T, *>

    //Dung constructor nay de init neu khong thay doi resouce va context

    init {
        this.resource = resource
        this.listItem = list
        this.layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    fun setNameField(prop: KProperty1<T, *>): BaseSpinnerAdapter<*> {
        nameProp = prop
        return this
    }

    fun setImageField(prop: KProperty1<T, *>): BaseSpinnerAdapter<*> {
        imageProp = prop
        return this
    }

    //VIDU Ve reference (get/set value theo variable name)
    fun <T> printProperty(instance: T, prop: KProperty1<T, *>) {
        Log.e(TAG, "${prop.name} = ${prop.get(instance)}")
    }

    fun <T> incrementProperty(
        instance: T, prop: KMutableProperty1<T, Int>
    ) {
        val value = prop.get(instance)
        prop.set(instance, value + 1)
//Vidu:
//        val person = Person("Lisa", 23)
//        printProperty(person, Person::name)
//        incrementProperty(person, Person::age)
    }

    override fun getCount(): Int {
        return listItem.count()
    }

    override fun getItem(position: Int): T? {
        return listItem[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return handleSpinnerView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return handleSpinnerView(position, convertView, parent)
    }

    fun handleSpinnerView(position: Int, convertView: View?, parent: ViewGroup): View {
        //Ham xu ly chung cho getView va getDropDownView
        val holder: ViewHolder
        val retView: View

        if (convertView == null) {
            retView = layoutInflater.inflate(R.layout.item_base_spinner, null)
            holder = ViewHolder()

            holder.image = retView.findViewById(R.id.image) as ImageView?
            holder.name = retView.findViewById(R.id.tvName) as MyTextView?

            retView.tag = holder

        } else {
            holder = convertView.tag as ViewHolder
            retView = convertView
        }

        //Set thong tin cho image va name
        val item = listItem[position]
        holder.name?.setText("${nameProp.get(item)}")
        holder.image?.visibility =
            View.GONE //Tam thoi khong xu ly image. Neu co image thi set o day

        return retView
    }

    internal class ViewHolder {
        var image: ImageView? = null
        var name: MyTextView? = null
    }
}