package com.datpham.basekotlin.base.customviews

import android.content.Context
import android.util.AttributeSet
import android.util.Log

class MyFilterTextView : MyTextView {
    constructor(context: Context) : super(context) {
        style(context, null)
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        style(context, attrs)
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        style(context, attrs)
        init()
    }

    private fun init() {
        this.isSelected = true
    }
}