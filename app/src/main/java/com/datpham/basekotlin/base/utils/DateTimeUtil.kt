package com.datpham.basekotlin.base.utils

import java.util.*

class DateTimeUtil {

    companion object {
        fun formatDate(time: Long): String {
            val date = Date(time)
            return date.toString("dd/MM/yyyy")
        }

    }

}