package com.datpham.basekotlin.base.utils

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.datpham.basekotlin.base.context.App.Companion.currentActivity
import com.datpham.basekotlin.base.customviews.MyButton
import com.datpham.modulo_test.R

class ConfirmDialog(message: String?, callback: () -> Unit) {

    init {
        val activity: Activity = currentActivity()
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_confirm)
        val tvContent = dialog.findViewById<TextView>(R.id.tvContent)
        val btnClose: MyButton = dialog.findViewById(R.id.btnClose)
        val btnConfirm: MyButton = dialog.findViewById(R.id.btnConfirm)
        tvContent.text = message
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        btnClose.setOnClickListener { view: View? -> dialog.cancel() }
        btnConfirm.setOnClickListener { view: View? ->
            callback.invoke()
            dialog.cancel()
        }
        dialog.show()
    }
}