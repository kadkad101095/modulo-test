package com.datpham.basekotlin.base.utils

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.enum.Language

private const val PREFERENCES_NAME = "MODULOTEST"

object PreferencesKeys {
    val LANGUAGE = stringPreferencesKey("language")
}

private val Context.dataStore by preferencesDataStore(PREFERENCES_NAME)

class DataStoreUtil {
    val dataStore = App.applicationContext().dataStore

    suspend fun saveLanguage(lang: Language) {
        dataStore.edit {
            it[PreferencesKeys.LANGUAGE] = lang.language
        }
    }

}