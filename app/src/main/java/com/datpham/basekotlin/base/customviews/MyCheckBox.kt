package com.datpham.basekotlin.base.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatCheckBox
import com.datpham.basekotlin.base.context.App.Companion.applicationContext
import com.datpham.modulo_test.R

class MyCheckBox : AppCompatCheckBox {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    private fun init() {
        if (this.typeface != null) {
            when (this.typeface.style) {
                Typeface.NORMAL -> {
                    val normal = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_regular)
                    )
                    setTypeface(normal, Typeface.NORMAL)
                }
                Typeface.BOLD -> {
                    val bold = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_bold)
                    )
                    setTypeface(bold, Typeface.BOLD)
                }
                Typeface.ITALIC -> {
                    val italic = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_italic)
                    )
                    setTypeface(italic, Typeface.ITALIC)
                }
                Typeface.BOLD_ITALIC -> {
                    val bold_italic = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_bold_italic)
                    )
                    setTypeface(bold_italic, Typeface.BOLD_ITALIC)
                }
            }
        } else {
            val normal = Typeface.createFromAsset(
                context.assets,
                applicationContext().getString(R.string.font_regular)
            )
            setTypeface(normal, Typeface.NORMAL)
        }
        setTextColor(resources.getColor(R.color.colorTextPrimary))
    }
}