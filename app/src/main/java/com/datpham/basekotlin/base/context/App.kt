package com.datpham.basekotlin.base.context

import android.content.Context
import android.content.res.Resources
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.datpham.basekotlin.local.AppDatabase

class App : MultiDexApplication() {

    lateinit var currentActivity: BaseActivity<*, *>

    override fun onCreate() {
        super.onCreate()
        instance = this
        MultiDex.install(this)
        AppDatabase.getInstance(this) //Init first database
    }

    companion object {
        private var instance: App? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }

        fun getResources(): Resources {
            return applicationContext().resources
        }

        fun currentActivity(): BaseActivity<*, *> {
            return instance!!.currentActivity
        }

        fun setCurrentActivity(activity: BaseActivity<*, *>) {
            instance!!.currentActivity = activity
        }
    }

}