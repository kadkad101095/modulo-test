package com.datpham.basekotlin.base.adapters

import android.view.View
import android.widget.AdapterView

open class BaseSpinnerItemSelectedListener(val onItemSelected: ((adapterView: AdapterView<*>, view: View, position: Int) -> Unit)?) :
    AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        onItemSelected?.invoke(p0!!, p1!!, p2)
    }
}
