package com.datpham.basekotlin.base.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.graphics.drawable.DrawableCompat
import androidx.viewpager.widget.ViewPager
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.context.App.Companion.applicationContext
import com.datpham.modulo_test.R
import com.google.android.material.tabs.TabLayout

class MyTabLayout : TabLayout {

    private var listImage: Array<Int>? = null

    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
    }

    fun addIcon(listImage: Array<Int>?) {
        this.listImage = listImage
    }

    private fun setFont(textView: TextView) {
        if (textView.typeface != null) {
            when (textView.typeface.style) {
                Typeface.NORMAL -> {
                    val normal = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_regular)
                    )
                    textView.setTypeface(normal, Typeface.NORMAL)
                }
                Typeface.BOLD -> {
                    val bold = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_bold)
                    )
                    textView.setTypeface(bold, Typeface.BOLD)
                }
                Typeface.ITALIC -> {
                    val italic = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_italic)
                    )
                    textView.setTypeface(italic, Typeface.ITALIC)
                }
                Typeface.BOLD_ITALIC -> {
                    val bold_italic = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_bold_italic)
                    )
                    textView.setTypeface(bold_italic, Typeface.BOLD_ITALIC)
                }
            }
        } else {
            val normal = Typeface.createFromAsset(
                context.assets,
                applicationContext().getString(R.string.font_regular)
            )
            textView.setTypeface(normal, Typeface.NORMAL)
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        val tabStrip = getChildAt(0) as ViewGroup
        val tabCount = tabStrip.childCount
        var tabView: ViewGroup
        var tabChildCount: Int
        var tabViewChild: View?
        for (i in 0 until tabCount) {
            tabView = tabStrip.getChildAt(i) as ViewGroup
            tabChildCount = tabView.childCount
            for (j in 0 until tabChildCount) {
                tabViewChild = tabView.getChildAt(j)
                if (tabViewChild is TextView) {
                    val tv = tabViewChild
                    setFont(tv)
                    tv.isAllCaps = false
                }
                if (tabViewChild is ImageView) {
                    val img = tabViewChild
                    img.layoutParams.width = 48
                    img.layoutParams.height = 48
                }
            }
        }
    }

    override fun addTab(tab: Tab, position: Int, setSelected: Boolean) {
        super.addTab(tab, position, setSelected)
        if (listImage != null && position < listImage!!.size) {
            tab.setIcon(listImage!![position])
        }
    }

    override fun setupWithViewPager(viewPager: ViewPager?) {
        super.setupWithViewPager(viewPager)
        if (viewPager == null) return
        val firstTab = getTabAt(0)
        if (firstTab != null && firstTab.icon != null) {
            DrawableCompat.setTint(
                firstTab.icon!!,
                App.getResources().getColor(R.color.colorAccent)
            )
        }
        addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: Tab) {
                if (tab.icon == null) return
                DrawableCompat.setTint(tab.icon!!, App.getResources().getColor(R.color.colorAccent))
            }

            override fun onTabUnselected(tab: Tab) {
                if (tab.icon == null) return
                DrawableCompat.setTint(tab.icon!!, App.getResources().getColor(R.color.colorText))
            }

            override fun onTabReselected(tab: Tab) {}
        })
    }
}