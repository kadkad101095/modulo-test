package com.datpham.basekotlin.base.enum

enum class DeviceType(val type: String) {
    LIGHT("Light"),
    HEATER("Heater"),
    ROLLER("RollerShutter")
}