package com.datpham.basekotlin.base.context

import androidx.databinding.ViewDataBinding

abstract class BaseAdapterViewModel : BaseViewModel() { //ViewModel cho adapter
    abstract fun newInstance(): BaseAdapterViewModel
    abstract fun <MODEL> bind(model: MODEL, position: Int)
    var binding: ViewDataBinding? = null
}