package com.datpham.basekotlin.base.utils

import android.graphics.*
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.SwitchCompat
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.customviews.MySeekArc
import com.datpham.modulo_test.R

/*
* RecyclerView
* */
@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

@BindingAdapter("layoutManager")
fun setLayoutManager(view: RecyclerView, layoutManager: RecyclerView.LayoutManager) {
    view.layoutManager = layoutManager
}

/*
* ImageView
* */

@BindingAdapter("android:src")
fun setImageViewResource(imageView: ImageView, resource: Int) {
    imageView.setImageResource(resource)
}

@BindingAdapter("setImageBitmap")
fun setImageBitmap(imageView: ImageView, bitmap: Bitmap?) {
    if (bitmap != null) {
        imageView.setImageBitmap(bitmap)
    }
}

@BindingAdapter("gifSrc")
fun gifSrc(imageView: ImageView, resource: Int) {
    Glide.with(App.applicationContext()).asGif().load(resource).into(imageView)
}

@BindingAdapter("circleImage")
fun setCircleImage(imageView: ImageView, url: String?) {
    if (url == null || url == "") return
    Glide.with(imageView)
        .load(url)
        .apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.ic_avatar_default))
        .into(imageView)
}

@BindingAdapter("normalImage")
fun setImageWithUrl(imageView: ImageView, url: String?) {
    if (url == null || url == "") return
    //Load anh binh thuong
    Glide.with(App.applicationContext())
        .load(url)
        .apply(RequestOptions.centerCropTransform().placeholder(R.mipmap.img_default))
        .into(imageView)
}

/*
* All Views
* */

@BindingAdapter("android:onClick")
fun onSingleClick(anyView: View, listener: View.OnClickListener) {
    val myClickListener = MyClickListener {
        listener.onClick(it)
    }
    anyView.setOnClickListener(myClickListener)
}

@BindingAdapter("visible")
fun setVisible(view: View, isVisible: Boolean) {
    if (isVisible) view.visibility = View.VISIBLE
    else view.visibility = View.GONE
}

@BindingAdapter("android:background")
fun setBackground(view: View, resource: Int) {
    view.background = ResourcesCompat.getDrawable(App.getResources(), resource, null)
}

@BindingAdapter("backgroundDrawableColor")
fun setBackgroundDrawableColor(view: View, resource: Int) {
    view.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
        ContextCompat.getColor(App.applicationContext(), resource),
        BlendModeCompat.SRC_ATOP
    )
}

@BindingAdapter("intAlpha")
fun setIntAlpha(view: View, alpha: Int) {
    view.alpha = alpha / 100f
}

/*
* SwitchCompat
* */
@BindingAdapter("thumbColor")
fun setSwitchCompatThumbColor(view: SwitchCompat, colorId: Int) {
    view.thumbDrawable.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
        ContextCompat.getColor(App.applicationContext(), colorId),
        BlendModeCompat.SRC_ATOP
    )
}

/*
* Motion layout
* */
@BindingAdapter("transitionListener")
fun setTransitionListener(view: MotionLayout, listener: MotionLayout.TransitionListener) {
    view.setTransitionListener(listener)
}

@BindingAdapter("transitionToEnd")
fun setTransitionToEnd(view: MotionLayout, toEnd: Boolean) {
    if (toEnd) {
        view.transitionToEnd()
    } else {
        view.transitionToStart()
    }
}

/*
* MySeekArc
* */
@BindingAdapter("seekArcProgressAttrChanged")
fun setSeekArcProgressAttrChanged(view: MySeekArc, listener: InverseBindingListener) {
    view.setOnSeekArcChangeListener(object : MySeekArc.OnSeekArcChangeListener {
        override fun onProgressChanged(seekArc: MySeekArc?, progress: Int, fromUser: Boolean) {
            listener.onChange()
        }

        override fun onStartTrackingTouch(seekArc: MySeekArc?) {

        }

        override fun onStopTrackingTouch(seekArc: MySeekArc?) {

        }
    })
}

@BindingAdapter("seekArcProgress")
fun setSeekArcProgress(view: MySeekArc, progress: Int) {
    view.progress = progress
}

@InverseBindingAdapter(attribute = "seekArcProgress")
fun getSeekArcProgress(view: MySeekArc): Int {
    return view.progress
}







