package com.datpham.basekotlin.base.utils

import android.os.SystemClock
import android.view.View

class MyClickListener(val callback: (view: View?) -> Unit) : View.OnClickListener {

    private var lastClickTime: Long = 0

    override fun onClick(view: View?) {
        if (SystemClock.elapsedRealtime() - lastClickTime < 100) {
            return
        }
        lastClickTime = SystemClock.elapsedRealtime()
        callback.invoke(view)
    }
}
