package com.datpham.basekotlin.base.customviews

import android.content.Context
import android.content.res.Resources.Theme
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatSpinner
import com.datpham.basekotlin.base.context.App.Companion.applicationContext
import com.datpham.modulo_test.R

class MySpinner : AppCompatSpinner {

    constructor(context: Context) : super(context) {
        setFontFromAsset(this)
    }

    constructor(context: Context, mode: Int) : super(context, mode) {
        setFontFromAsset(this)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setFontFromAsset(this)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        setFontFromAsset(this)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, mode: Int) : super(
        context,
        attrs,
        defStyleAttr,
        mode
    ) {
        setFontFromAsset(this)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        mode: Int,
        popupTheme: Theme?
    ) : super(context, attrs, defStyleAttr, mode, popupTheme) {
        setFontFromAsset(this)
    }

    override fun setSelection(position: Int, animate: Boolean) {
        val sameSelected = position == selectedItemPosition
        super.setSelection(position, animate)
        if (sameSelected) {
            // Spinner does not call the OnItemSelectedListener if the same item is selected, so do it manually now
            onItemSelectedListener!!.onItemSelected(this, selectedView, position, selectedItemId)
        }
    }

    override fun setSelection(position: Int) {
        val sameSelected = position == selectedItemPosition
        super.setSelection(position)
        if (sameSelected) {
            // Spinner does not call the OnItemSelectedListener if the same item is selected, so do it manually now
            onItemSelectedListener!!.onItemSelected(this, selectedView, position, selectedItemId)
        }
    }

    fun setFontFromAsset(view: View) {
        var strFont: String? = null
        var tfFontFace: Typeface? = null
        val strButton = MyButton::class.java.canonicalName
        val strTextView = MyTextView::class.java.canonicalName
        val strEditText = MyEditText::class.java.canonicalName
        val strView = view.javaClass.canonicalName
        try {
            if (view.isInEditMode) {
                return
            }
            strFont = applicationContext().getString(R.string.font_regular)
            tfFontFace = Typeface.createFromAsset(context.assets, strFont)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            if (strFont != null && tfFontFace != null) {
                if (strView == strButton) {
                    (view as MyButton).setTypeface(tfFontFace)
                } else if (strView == strTextView) {
                    (view as MyButton).setTypeface(tfFontFace)
                } else if (strView == strEditText) {
                    (view as MyButton).setTypeface(tfFontFace)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
