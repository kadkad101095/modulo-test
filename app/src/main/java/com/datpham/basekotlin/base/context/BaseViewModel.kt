package com.datpham.basekotlin.base.context

import android.content.Intent
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.datpham.basekotlin.network.ApiManager

abstract class BaseViewModel : ViewModel() { //ViewModel cho Activity va Fragment

    val apiManager = ApiManager()

    fun showToast(message: String, isSuccess: Boolean = false) {
        App.currentActivity().showToast(message, isSuccess)
    }

    fun showToast(messageId: Int, isSuccess: Boolean = false) {
        App.currentActivity().showToast(messageId, isSuccess)
    }

    fun showLogE(message: String) {
        App.currentActivity().showLogE(message)
    }

    fun showLogD(message: String) {
        App.currentActivity().showLogD(message)
    }

    fun openFragment(fragmentId: Int, passData: Bundle? = null) {
        App.currentActivity().openFragment(fragmentId, passData)
    }

    fun showLoading() {
        App.currentActivity().loadingDialog.show()
    }

    fun hideLoading() {
        App.currentActivity().loadingDialog.dismiss()
    }

    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    open fun onStart() {

    }

    open fun onResume() {

    }

    open fun onPause() {

    }

    open fun onDestroy() {

    }
}