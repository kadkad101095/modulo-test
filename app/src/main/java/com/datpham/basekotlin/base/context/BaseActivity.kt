package com.datpham.basekotlin.base.context

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.datpham.basekotlin.base.utils.*
import com.datpham.basekotlin.base.utils.LoadingDialog
import com.datpham.basekotlin.base.utils.NotyDialog
import com.datpham.modulo_test.R
import com.datpham.modulo_test.BR


abstract class BaseActivity<VIEWDATABINDING : ViewDataBinding, VIEWMODEL : BaseViewModel> :
    AppCompatActivity() {

    lateinit var mBinding: VIEWDATABINDING
    lateinit var mViewModel: VIEWMODEL
    lateinit var loadingDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, getLayout())
        mViewModel = ViewModelProvider(this).get(getViewModel())
        mBinding.setVariable(BR.viewModel, mViewModel)
        mBinding.lifecycleOwner = this
        loadingDialog = LoadingDialog(this)
        App.setCurrentActivity(this)
        initComponents()
    }

    protected abstract fun getLayout(): Int

    protected abstract fun getViewModel(): Class<VIEWMODEL>

    protected abstract fun initComponents()

    open fun openFragment(fragmentId: Int, passData: Bundle? = null) {
        findNavController(R.id.nav_host_fragment).navigate(fragmentId, passData)
    }

    fun showLoading() {
        loadingDialog.show()
    }

    fun hideLoading() {
        loadingDialog.dismiss()
    }

    fun showToast(message: String, success: Boolean = false) {
        loadingDialog.dismiss()
        NotyDialog(message, success)
    }

    fun showToast(messageId: Int, success: Boolean = false) {
        loadingDialog.dismiss()
        NotyDialog(getString(messageId), success)
    }

    fun showLogE(message: String) {
        Log.e(TAG, message)
    }

    fun showLogD(message: String) {
        Log.d(TAG, message)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        //Hide keyboard when click outside edittext
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onStart() {
        super.onStart()
        App.setCurrentActivity(this)
        mViewModel.onStart()
    }

    override fun onResume() {
        super.onResume()
        mViewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        mViewModel.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewModel.onDestroy()
    }

    override fun onBackPressed() {
        if (findNavController(R.id.nav_host_fragment).graph.startDestination == findNavController(R.id.nav_host_fragment).currentDestination?.id) {
            ConfirmDialog(getString(R.string.confirm_exit_application)) {
                finish()
            }
        } else {
            super.onBackPressed()
        }
    }

}
