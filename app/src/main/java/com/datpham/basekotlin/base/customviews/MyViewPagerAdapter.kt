package com.datpham.basekotlin.base.customviews

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.viewpager.widget.PagerAdapter
import com.datpham.basekotlin.base.context.BaseViewModel
import com.datpham.modulo_test.BR

class MyViewPagerAdapter() : PagerAdapter() {

    companion object {
        fun newInstance(
            layouts: ArrayList<Pair<Int, BaseViewModel?>>,
            pageTitle: Array<String>? = null
        ): MyViewPagerAdapter {
            val myViewPagerAdapter = MyViewPagerAdapter()
            myViewPagerAdapter.layouts = layouts
            myViewPagerAdapter.pageTitle = pageTitle
            return myViewPagerAdapter
        }
    }

    private var layouts = ArrayList<Pair<Int, BaseViewModel?>>()
    private var pageTitle: Array<String>? = null

    override fun getPageTitle(position: Int): CharSequence? {
        if (pageTitle != null && position < pageTitle!!.size) {
            return pageTitle!![position]
        }
        return super.getPageTitle(position)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }

    override fun getCount(): Int {
        return layouts.size
    }

    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        parent.removeView(`object` as View)
    }

    override fun instantiateItem(viewGroup: ViewGroup, position: Int): Any {
        val mBinding: ViewDataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(viewGroup.context),
            layouts[position].first,
            null,
            false
        )
        viewGroup.addView(mBinding.root)
        if (layouts[position].second != null) {
            mBinding.setVariable(BR.viewModel, layouts[position].second)
            mBinding.executePendingBindings()
        }
        return mBinding.root
    }
}