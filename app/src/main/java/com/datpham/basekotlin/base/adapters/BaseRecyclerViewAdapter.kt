package com.datpham.basekotlin.base.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.datpham.basekotlin.base.context.BaseAdapterViewModel
import com.datpham.modulo_test.BR
import java.util.ArrayList

class BaseRecyclerViewAdapter<MODEL, VIEWDATABINDING : ViewDataBinding> :
    RecyclerView.Adapter<ViewHolder>(), Filterable {

    var rows = ArrayList<BaseRecyclerViewRow>() //Thong tin cua 1 row
    var items = ArrayList<MODEL>() //List data cua recyclerview
    var inflater: LayoutInflater? = null

    //Filter
    var customFilter: CustomFilter? = null

    override fun getItemViewType(position: Int): Int {
        val item = items.get(position)
        if (item is BaseItem) {
            return item.getViewType()
        } else {
            return 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (rows.size == 0) {
            throw Throwable("Please add a RecyclerView Row")
        }
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.context)
        }
        for (row in rows) {
            if (viewType == row.viewType) {
                val binding: VIEWDATABINDING =
                    DataBindingUtil.inflate(inflater!!, row.layoutId, parent, false)
                return ViewHolder(binding, row.viewModel!!)
            }
        }
        //Mac dinh lay row dau tien
        val firstRow = rows.get(0)
        val binding: VIEWDATABINDING =
            DataBindingUtil.inflate(inflater!!, firstRow.layoutId, parent, false)
        return ViewHolder(binding, firstRow.viewModel!!)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items.get(position), position)
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                filterResults.values = customFilter?.getFilterdList()
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results?.values == null) {
                    return
                }
                items = results.values as ArrayList<MODEL>
                notifyDataSetChanged()
            }

        }
    }

}

class ViewHolder(
    private val binding: ViewDataBinding,
    private var viewModelType: BaseAdapterViewModel
) :
    RecyclerView.ViewHolder(binding.root) {

    lateinit var viewModel: BaseAdapterViewModel

    fun <MODEL> bind(model: MODEL, position: Int) {
        viewModel = viewModelType.newInstance()
        viewModel.binding = binding
        viewModel.bind(model, position)
        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()
    }
}


interface BaseItem { //Neu muon su dung viewType thi phai implement interface nay
    fun getViewType(): Int
}

interface CustomFilter { //Use for filter adapter
    fun getFilterdList(): List<Any>
}