package com.datpham.basekotlin.base.enum

enum class Language(val language: String) {
    EN("en"),
    FR("fr")
}