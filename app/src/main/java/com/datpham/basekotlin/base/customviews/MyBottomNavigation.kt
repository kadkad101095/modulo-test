package com.datpham.basekotlin.base.customviews

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.datpham.basekotlin.base.context.App.Companion.applicationContext
import com.datpham.modulo_test.R
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.internal.BaselineLayout
import java.lang.reflect.Field

class MyBottomNavigation : BottomNavigationView {

    private var fontFace: Typeface? = null

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

    }

    @SuppressLint("RestrictedApi")
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        val bottomMenu = getChildAt(0) as ViewGroup
        val bottomMenuChildCount = bottomMenu.childCount
        var item: BottomNavigationItemView
        var itemTitle: View
        val shiftingMode: Field
        if (fontFace == null) {
            fontFace = Typeface.createFromAsset(
                context!!.assets,
                applicationContext().getString(R.string.font_regular)
            )
        }
        try {
            //if you want to disable shiftingMode:
            //shiftingMode is a private member variable so you have to get access to it like this:
            shiftingMode = bottomMenu.javaClass.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(bottomMenu, false)
            shiftingMode.isAccessible = false
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
        for (i in 0 until bottomMenuChildCount) {
            item = bottomMenu.getChildAt(i) as BottomNavigationItemView
            //this shows all titles of items
            item.setChecked(true)
            //every BottomNavigationItemView has two children, first is an itemIcon and second is an itemTitle
            itemTitle = item.getChildAt(1)
            //every itemTitle has two children, first is a smallLabel and second is a largeLabel. these two are type of AppCompatTextView
            ((itemTitle as BaselineLayout).getChildAt(0) as TextView).setTypeface(
                fontFace,
                Typeface.NORMAL
            )
            (itemTitle.getChildAt(1) as TextView).setTypeface(fontFace, Typeface.BOLD)
            (itemTitle.getChildAt(0) as TextView).textSize = 10f
            (itemTitle.getChildAt(1) as TextView).textSize = 10f
        }
    }
}