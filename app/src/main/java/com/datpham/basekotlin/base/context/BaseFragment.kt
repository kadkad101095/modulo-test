package com.datpham.basekotlin.base.context

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.datpham.modulo_test.BR

abstract class BaseFragment<VIEWDATABINDING : ViewDataBinding, VIEWMODEL : BaseViewModel> :
    Fragment() {

    lateinit var mBinding: VIEWDATABINDING
    lateinit var mViewModel: VIEWMODEL

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mViewModel = ViewModelProvider(this).get(getViewModel())
        return if (::mBinding.isInitialized) {
            mBinding.root
        } else {
            mBinding = DataBindingUtil.inflate(inflater, getLayout(), container, false)
            mBinding.setVariable(BR.viewModel, mViewModel)
            mBinding.lifecycleOwner = this
            with(mBinding) {
                root
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initComponents()
    }

    override fun onStart() {
        super.onStart()
        mViewModel.onStart()
    }

    override fun onResume() {
        super.onResume()
        mViewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        mViewModel.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewModel.onDestroy()
    }

    protected abstract fun getLayout(): Int

    protected abstract fun getViewModel(): Class<VIEWMODEL>

    abstract fun initComponents()

}