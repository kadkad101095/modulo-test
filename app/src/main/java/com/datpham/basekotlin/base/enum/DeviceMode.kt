package com.datpham.basekotlin.base.enum

enum class DeviceMode(val type: String) {
    ON("ON"),
    OFF("OFF")
}