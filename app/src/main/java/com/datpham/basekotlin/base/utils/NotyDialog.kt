package com.datpham.basekotlin.base.utils

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.datpham.basekotlin.base.context.App.Companion.currentActivity
import com.datpham.basekotlin.base.customviews.MyButton
import com.datpham.modulo_test.R

class NotyDialog(message: String?, isSucess: Boolean = false) {
    init {
        val activity: Activity = currentActivity()
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(R.color.white_transparent)
        dialog.setContentView(R.layout.dialog_noty)
        val tvContent = dialog.findViewById<TextView>(R.id.tvContent)
        val btnClose: MyButton = dialog.findViewById(R.id.btnClose)
        val imgNotify = dialog.findViewById<ImageView>(R.id.imgNotify)
        if (isSucess) {
            imgNotify.setImageResource(R.drawable.ic_success)
        } else {
            imgNotify.setImageResource(R.drawable.ic_error)
        }
        tvContent.text = message
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        btnClose.setOnClickListener { view: View? -> dialog.cancel() }
        dialog.show()
    }
}