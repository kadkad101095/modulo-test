package com.datpham.basekotlin.base.utils

import com.google.gson.Gson

class GsonUtil {
    companion object {
        var gson = Gson()

        fun toJson(myObject: Any?): String {
            if (myObject == null) {
                return ""
            }
            return gson.toJson(myObject)
        }

        fun <T> fromJson(json: String, type: Class<T>): T? {
            return gson.fromJson(json, type)
        }
    }
}