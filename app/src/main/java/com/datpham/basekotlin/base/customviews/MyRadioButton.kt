package com.datpham.basekotlin.base.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatRadioButton
import com.datpham.basekotlin.base.context.App.Companion.applicationContext
import com.datpham.modulo_test.R

class MyRadioButton : AppCompatRadioButton {

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?) : super(context) {
        init(null)
    }

    private fun init(attrs: AttributeSet?) {
        if (this.typeface != null) {
            when (this.typeface.style) {
                Typeface.NORMAL -> {
                    val normal = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_regular)
                    )
                    setTypeface(normal, Typeface.NORMAL)
                }
                Typeface.BOLD -> {
                    val bold = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_bold)
                    )
                    setTypeface(bold, Typeface.BOLD)
                }
                Typeface.ITALIC -> {
                    val italic = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_italic)
                    )
                    setTypeface(italic, Typeface.ITALIC)
                }
                Typeface.BOLD_ITALIC -> {
                    val bold_italic = Typeface.createFromAsset(
                        context.assets,
                        applicationContext().getString(R.string.font_bold_italic)
                    )
                    setTypeface(bold_italic, Typeface.BOLD_ITALIC)
                }
            }
        } else {
            val normal = Typeface.createFromAsset(
                context.assets,
                applicationContext().getString(R.string.font_regular)
            )
            setTypeface(normal, Typeface.NORMAL)
        }
    }
}