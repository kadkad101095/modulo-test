package com.datpham.basekotlin.base.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import com.datpham.basekotlin.base.customviews.MyTextView
import com.datpham.modulo_test.R
import java.lang.ref.WeakReference

class LoadingDialog(context: Activity) {
    private var mDialog: Dialog? = null
    private var mContext: WeakReference<Activity>? = null
    private var tvProgress: MyTextView? = null

    init {
        mContext = WeakReference(context)
        initDialog()
        initView()
    }

    private fun initDialog() {
        var context: Context
        mDialog = Dialog(mContext!!.get()!!, R.style.base_progess)
        mDialog?.setContentView(R.layout.dialog_loading)
        if (mDialog!!.window != null) {
            mDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        mDialog!!.setCancelable(true)
        mDialog!!.setCanceledOnTouchOutside(true)
    }

    private fun initView() {
        tvProgress = mDialog!!.findViewById(R.id.tvProgress)
    }

    fun show() {
        try {
            if (mDialog == null || isShowing()) {
                return
            }
            mDialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dismiss() {
        if (mDialog == null || !isShowing()) {
            return
        }
        mDialog!!.dismiss()
        tvProgress!!.text = "0%"
        tvProgress!!.visibility = View.GONE
    }

    fun updateProgress(value: String?) {
        if (mDialog == null || !isShowing()) {
            return
        }
        tvProgress!!.text = value
        tvProgress!!.visibility = View.VISIBLE
    }

    private fun isShowing(): Boolean {
        return mDialog != null && mDialog!!.isShowing
    }
}