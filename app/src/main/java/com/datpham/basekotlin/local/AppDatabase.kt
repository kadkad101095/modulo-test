package com.datpham.basekotlin.local

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.datpham.basekotlin.base.enum.DeviceType
import com.datpham.basekotlin.base.utils.DATABASE_NAME
import com.datpham.basekotlin.base.utils.TAG
import com.datpham.basekotlin.network.entities.Device
import com.datpham.basekotlin.network.entities.Heater
import com.datpham.basekotlin.network.entities.Light
import com.datpham.basekotlin.network.entities.RollerShutter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

@SuppressLint("CheckResult")
@Database(entities = [Light::class, Heater::class, RollerShutter::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun deviceDao(): DeviceDao

    companion object {

        private var dbInstance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            dbInstance ?: synchronized(this) {
                dbInstance ?: buildDatabaseInstance(context).also {
                    dbInstance = it
                }
            }

        private fun buildDatabaseInstance(context: Context) =
            Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    private val errorHandler = Consumer<Throwable?> {
        Log.e(TAG, it.message ?: "ERROR")
    }

    fun getAllDevices(success: Consumer<List<Device>>) {
        Observable.zip(
            deviceDao().getAllLight().observeOn(Schedulers.io()),
            deviceDao().getAllHeater().observeOn(Schedulers.io()),
            deviceDao().getAllRollerShutter().observeOn(Schedulers.io()),
            { lights, heaters, rollers ->
                val listDevices = ArrayList<Device>()
                listDevices.addAll(lights)
                listDevices.addAll(heaters)
                listDevices.addAll(rollers)
                listDevices
            }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun getDevice(id: String, type: String, success: Consumer<Device>) {
        when (type) {
            DeviceType.LIGHT.type -> getLight(id, Consumer {
                success.accept(it)
            })
            DeviceType.HEATER.type -> getHeater(id, Consumer {
                success.accept(it)
            })
            DeviceType.ROLLER.type -> getRollerShutter(id, Consumer {
                success.accept(it)
            })
        }
    }

    fun getLight(id: String, success: Consumer<Light>) {
        deviceDao().getLight(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun getHeater(id: String, success: Consumer<Heater>) {
        deviceDao().getHeater(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun getRollerShutter(id: String, success: Consumer<RollerShutter>) {
        deviceDao().getRollerShutter(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun insertDevice(device: Device, success: Action) {
        when (device) {
            is Light -> {
                deviceDao().insertLights(device)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success, errorHandler)
            }
            is Heater -> {
                deviceDao().insertHeaters(device)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success, errorHandler)
            }
            is RollerShutter -> {
                deviceDao().insertRollerShutters(device)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success, errorHandler)
            }
        }
    }
}