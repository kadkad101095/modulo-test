package com.datpham.basekotlin.local

import androidx.room.*
import com.datpham.basekotlin.network.entities.Heater
import com.datpham.basekotlin.network.entities.Light
import com.datpham.basekotlin.network.entities.RollerShutter
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface DeviceDao {

    @Query("SELECT * FROM light")
    fun getAllLight(): Observable<List<Light>>

    @Query("SELECT * FROM light WHERE _id == :id LIMIT 1")
    fun getLight(id: String): Single<Light>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLights(vararg lights: Light): Completable

    @Update
    fun updateLight(light: Light): Completable

    @Delete
    fun deleteLight(light: Light): Completable

    @Query("SELECT * FROM heater")
    fun getAllHeater(): Observable<List<Heater>>

    @Query("SELECT * FROM heater WHERE _id == :id LIMIT 1")
    fun getHeater(id: String): Single<Heater>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHeaters(vararg heaters: Heater): Completable

    @Update
    fun updateHeater(heater: Heater): Completable

    @Delete
    fun deleteHeater(heater: Heater): Completable

    @Query("SELECT * FROM rollershutter")
    fun getAllRollerShutter(): Observable<List<RollerShutter>>

    @Query("SELECT * FROM rollershutter WHERE _id == :id LIMIT 1")
    fun getRollerShutter(id: String): Single<RollerShutter>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRollerShutters(vararg rollerShutters: RollerShutter): Completable

    @Update
    fun updateRollerShutter(rollerShutter: RollerShutter): Completable

    @Delete
    fun deleteRollerShutter(rollerShutter: RollerShutter): Completable

}