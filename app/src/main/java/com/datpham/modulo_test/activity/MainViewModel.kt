package com.datpham.modulo_test.activity

import androidx.lifecycle.MutableLiveData
import com.datpham.basekotlin.base.context.BaseViewModel
import com.datpham.basekotlin.network.entities.Device
import com.datpham.basekotlin.network.entities.User

class MainViewModel : BaseViewModel() {
    val userData = MutableLiveData<User>()
    val deviceData = MutableLiveData<Device>()
}