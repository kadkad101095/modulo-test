package com.datpham.modulo_test.activity

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import androidx.lifecycle.asLiveData
import com.datpham.basekotlin.base.context.BaseActivity
import com.datpham.basekotlin.base.enum.Language
import com.datpham.basekotlin.base.utils.ContextUtils
import com.datpham.basekotlin.base.utils.DataStoreUtil
import com.datpham.basekotlin.base.utils.PreferencesKeys
import com.datpham.modulo_test.R
import com.datpham.modulo_test.databinding.ActivityMainBinding
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import java.util.*

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    private val dataStoreUtil = DataStoreUtil()

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun getViewModel(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun initComponents() {

    }

    override fun attachBaseContext(newBase: Context) {
        val preference = runBlocking { dataStoreUtil.dataStore.data.first() }
        val lang = preference[PreferencesKeys.LANGUAGE] ?: Language.EN.language
        val localeUpdatedContext: ContextWrapper =
            ContextUtils.updateLocale(newBase, Locale(lang))
        super.attachBaseContext(localeUpdatedContext)
    }

}