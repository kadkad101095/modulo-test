package com.datpham.modulo_test.fragment.listimage

import com.datpham.basekotlin.base.context.BaseFragment
import com.datpham.modulo_test.R
import com.datpham.modulo_test.databinding.FragmentListImageBinding

class ListImageFragment : BaseFragment<FragmentListImageBinding, ListImageViewModel>() {

    override fun getLayout(): Int {
        return R.layout.fragment_list_image
    }

    override fun getViewModel(): Class<ListImageViewModel> {
        return ListImageViewModel::class.java
    }

    override fun initComponents() {
        mViewModel.initData()
    }
    
}