package com.datpham.modulo_test.fragment.listimage

import androidx.recyclerview.widget.LinearLayoutManager
import com.datpham.basekotlin.base.adapters.RecyclerViewBuilder
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.context.BaseViewModel
import com.datpham.modulo_test.R

class ListImageViewModel : BaseViewModel() {

    val listImage = ArrayList<String>()
    val layoutManager = LinearLayoutManager(App.applicationContext())
    val adapter = RecyclerViewBuilder()
        .addRow(R.layout.item_list_image, ListImageAdapterVM())
        .build(listImage)

    fun initData() {
        showLoading()
        apiManager.getAllImage {
            hideLoading()
            listImage.addAll(it.data)
            adapter.notifyDataSetChanged()
        }
    }

}