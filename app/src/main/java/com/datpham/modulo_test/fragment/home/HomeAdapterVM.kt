package com.datpham.modulo_test.fragment.home

import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.datpham.basekotlin.base.context.BaseAdapterViewModel
import com.datpham.basekotlin.base.enum.DeviceMode
import com.datpham.basekotlin.base.utils.HEATER_MIN_TEMP
import com.datpham.basekotlin.base.utils.HEATER_STEP
import com.datpham.basekotlin.network.entities.Device
import com.datpham.basekotlin.network.entities.Heater
import com.datpham.basekotlin.network.entities.Light
import com.datpham.basekotlin.network.entities.RollerShutter
import com.datpham.modulo_test.R


class HomeAdapterVM(val callback: Callback) : BaseAdapterViewModel() {

    lateinit var device: Device
    lateinit var status: String
    var position = 0
    var icon = R.mipmap.ic_light
    var color = R.color.color_home_light
    var switchColor = ObservableField(R.color.color_home_light)
    var isShowSwitch = true
    var isOn = true

    override fun newInstance(): BaseAdapterViewModel {
        return HomeAdapterVM(callback)
    }

    override fun <MODEL> bind(model: MODEL, position: Int) {
        device = model as Device
        this.position = position
        when (val device = device) {
            is Light -> {
                icon = R.mipmap.ic_light
                color = R.color.color_home_light
                status = "${device.intensity}%"
                isShowSwitch = true
                isOn = device.mode == DeviceMode.ON.type
                switchColor.set(if (isOn) R.color.color_home_light else R.color.grey_dark)
            }
            is Heater -> {
                icon = R.mipmap.ic_heater
                color = R.color.color_home_heater
                status = "${HEATER_MIN_TEMP + (HEATER_STEP * device.temperature)}°C"
                isShowSwitch = true
                isOn = device.mode == DeviceMode.ON.type
                switchColor.set(if (isOn) R.color.color_home_heater else R.color.grey_dark)
            }
            is RollerShutter -> {
                icon = R.mipmap.ic_rollershutter
                color = R.color.color_home_roller
                status = "${device.position}%"
                isShowSwitch = false
            }
        }
        //New version handle only
        status = "Using port: ${device.port}"
        isOn = device.state == "ON"
    }

    fun onDeleteClick(view: View) {
        callback.onItemDeleteClick(position, device)
    }

    fun onItemClick(view: View) {
//        callback.onItemClick(device)
    }

    fun onChangeModeClick(view: View) {
        callback.onItemChangeMode(device, isOn)
        when (device) {
            is Light -> switchColor.set(if (isOn) R.color.color_home_light else R.color.grey_dark)
            is Heater -> switchColor.set(if (isOn) R.color.color_home_heater else R.color.grey_dark)
        }
    }

    interface Callback {
        fun onItemClick(device: Device)
        fun onItemDeleteClick(position: Int, device: Device)
        fun onItemChangeMode(device: Device, isOn: Boolean)
    }

}