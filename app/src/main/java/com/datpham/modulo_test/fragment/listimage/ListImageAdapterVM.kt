package com.datpham.modulo_test.fragment.listimage

import com.datpham.basekotlin.base.context.BaseAdapterViewModel
import com.datpham.basekotlin.base.utils.BASE_URL

class ListImageAdapterVM: BaseAdapterViewModel() {

    var image = ""

    override fun newInstance(): BaseAdapterViewModel {
        return ListImageAdapterVM()
    }

    override fun <MODEL> bind(model: MODEL, position: Int) {
        image = "$BASE_URL/${model as String}"
    }

}