package com.datpham.modulo_test.fragment.home

import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.forEach
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.datpham.basekotlin.base.adapters.CustomFilter
import com.datpham.basekotlin.base.adapters.RecyclerViewBuilder
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.context.BaseViewModel
import com.datpham.basekotlin.base.enum.DeviceMode
import com.datpham.basekotlin.base.enum.DeviceType
import com.datpham.basekotlin.base.enum.Language
import com.datpham.basekotlin.base.utils.ConfirmDialog
import com.datpham.basekotlin.base.utils.DataStoreUtil
import com.datpham.basekotlin.local.AppDatabase
import com.datpham.basekotlin.network.entities.*
import com.datpham.modulo_test.R
import com.datpham.modulo_test.activity.MainViewModel
import kotlinx.coroutines.runBlocking

class HomeViewModel : BaseViewModel(), HomeAdapterVM.Callback {

    private val shareViewModel = App.currentActivity().mViewModel as MainViewModel
    private val dataStoreUtil = DataStoreUtil()
    private val appDatabase = AppDatabase.getInstance(App.applicationContext())

    val listDevices = ArrayList<Device>()
    val layoutManagerDevices = LinearLayoutManager(App.applicationContext())
    val adapterDevices = RecyclerViewBuilder()
        .addRow(R.layout.item_home, HomeAdapterVM(this))
        .build(listDevices)

    private val listFilterCondition = ArrayList<String>() //List used to save filter condition
    private var popupFilter: PopupMenu? = null

    val user = MutableLiveData<User>()

    //When ready all event start here
    fun initData() {
        getListDevices()
        initFilterList()
        initFilterCondition()
    }

    //Get list devices and user information from backend
    private fun getListDevices() {
        if (listDevices.isEmpty()) {
            showLoading()
            apiManager.getData {
//                if (user.value == null) { //Update only when no user information yet
//                    user.postValue(it.user)
//                }
                user.postValue(User("Dat", "Pham", Address(), 123))
                listDevices.clear()
                listDevices.addAll(it.data)
                adapterDevices.notifyDataSetChanged()
                reloadDeviceLocalState()
                hideLoading()
            }
        } else {
            reloadDeviceLocalState()
        }
    }

    //Reload local state of device when user interacted before
    private fun reloadDeviceLocalState() {
        appDatabase.getAllDevices {
            val stateListIndex = it.groupBy {
                it._id
            }
            listDevices.forEach { device ->
                when (val state = stateListIndex.get(device._id)?.firstOrNull()) {
                    is Light -> {
                        device as Light
                        device.mode = state.mode
                        device.intensity = state.intensity
                    }
                    is Heater -> {
                        device as Heater
                        device.temperature = state.temperature
                        device.mode = state.mode
                    }
                    is RollerShutter -> {
                        device as RollerShutter
                        device.position = state.position
                    }
                }
            }
            adapterDevices.notifyDataSetChanged()
        }
    }

    private fun initFilterList() {
        listFilterCondition.clear()
        if (popupFilter == null) {
            //Default is show all devices
            listFilterCondition.add(DeviceType.LIGHT.type)
            listFilterCondition.add(DeviceType.HEATER.type)
            listFilterCondition.add(DeviceType.ROLLER.type)
        } else {
            //If popup existed -> filter condition existed, keep its state
            popupFilter?.menu?.forEach { item ->
                when (item.itemId) {
                    R.id.filter_light -> editFilterCondition(item.isChecked, DeviceType.LIGHT)
                    R.id.filter_heater -> editFilterCondition(item.isChecked, DeviceType.HEATER)
                    R.id.filter_roller -> editFilterCondition(item.isChecked, DeviceType.ROLLER)
                }
            }
        }
    }

    //Add condition to list devices adapter filter
    private fun initFilterCondition() {
        adapterDevices.customFilter = object : CustomFilter {
            override fun getFilterdList(): List<Any> {
                return listDevices.filter {
                    return@filter listFilterCondition.contains(it.productType)
                }
            }
        }
    }

    private fun editFilterCondition(isSelected: Boolean, deviceType: DeviceType) {
        if (isSelected) {
            if (deviceType.type !in listFilterCondition) {
                listFilterCondition.add(deviceType.type)
            }
        } else {
            listFilterCondition.remove(deviceType.type)
        }
    }

    fun showFilterPopup(view: View) {
        if (popupFilter == null) { //Init first time click to detect anchor view
            popupFilter = PopupMenu(App.currentActivity(), view)
            popupFilter!!.inflate(R.menu.menu_filter_home)
            popupFilter!!.menu.forEach {
                it.isCheckable = true
                it.isChecked = true //Default is show all devices
            }
            popupFilter?.setOnMenuItemClickListener { item: MenuItem ->
                item.isChecked = !item.isChecked
                item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
                item.actionView = View(App.currentActivity())
                when (item.itemId) {
                    R.id.filter_light -> editFilterCondition(item.isChecked, DeviceType.LIGHT)
                    R.id.filter_heater -> editFilterCondition(item.isChecked, DeviceType.HEATER)
                    R.id.filter_roller -> editFilterCondition(item.isChecked, DeviceType.ROLLER)
                }
                adapterDevices.filter.filter(null)
                false
            }
        }
        popupFilter!!.show()
    }

    fun onProfileClick(view: View) {
//        shareViewModel.userData.postValue(user.value)
//        openFragment(R.id.action_homeFragment_to_profileFragment)
        openFragment(R.id.action_homeFragment_to_listImageFragment)
    }

    override fun onItemClick(device: Device) {
        shareViewModel.deviceData.postValue(device)
        appDatabase.getDevice(device._id, device.productType) { stateDevice ->
            shareViewModel.deviceData.postValue(stateDevice) //If there is local data -> Update current device
        }
        openFragment(R.id.action_homeFragment_to_levelFragment)
    }

    override fun onItemDeleteClick(position: Int, device: Device) {
        ConfirmDialog(App.currentActivity().getString(R.string.confirm_delete_device)) {
            listDevices.remove(device)
            adapterDevices.items.remove(device)
            adapterDevices.notifyItemRemoved(position)
            adapterDevices.notifyItemRangeChanged(position, adapterDevices.items.size)
        }
    }

    override fun onItemChangeMode(device: Device, isOn: Boolean) {
//        val mode = if (isOn) DeviceMode.ON.type else DeviceMode.OFF.type
//        when (device) {
//            is Light -> device.mode = mode
//            is Heater -> device.mode = mode
//        }
//        appDatabase.insertDevice(device) {}
        showLoading()
        device.state = if (isOn) "ON" else "OFF"
        apiManager.updateDevice(device) {
            hideLoading()
            listDevices.clear()
            getListDevices()
        }
    }

    fun onLanguageChangeEn(view: View) {
        ConfirmDialog(App.currentActivity().getString(R.string.confirm_change_to_eng)) {
            runBlocking { dataStoreUtil.saveLanguage(Language.EN) }
            reloadActivity()
        }
    }

    fun onLanguageChangeFr(view: View) {
        ConfirmDialog(App.currentActivity().getString(R.string.confirm_change_to_fr)) {
            runBlocking { dataStoreUtil.saveLanguage(Language.FR) }
            reloadActivity()
        }
    }

    private fun reloadActivity() {
        val activity = App.currentActivity()
        activity.finish()
        activity.startActivity(activity.intent)
    }

}