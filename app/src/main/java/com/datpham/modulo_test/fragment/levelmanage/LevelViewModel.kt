package com.datpham.modulo_test.fragment.levelmanage

import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.context.BaseViewModel
import com.datpham.basekotlin.base.enum.DeviceMode
import com.datpham.basekotlin.base.utils.HEATER_MAX_TEMP
import com.datpham.basekotlin.base.utils.HEATER_MIN_TEMP
import com.datpham.basekotlin.base.utils.HEATER_STEP
import com.datpham.basekotlin.local.AppDatabase
import com.datpham.basekotlin.network.entities.Device
import com.datpham.basekotlin.network.entities.Heater
import com.datpham.basekotlin.network.entities.Light
import com.datpham.basekotlin.network.entities.RollerShutter
import com.datpham.modulo_test.R
import com.datpham.modulo_test.activity.MainViewModel

class LevelViewModel : BaseViewModel() {

    private val shareViewModel = App.currentActivity().mViewModel as MainViewModel
    private val appDatabase = AppDatabase.getInstance(App.applicationContext())

    val name = MutableLiveData<String>()
    val iconType = MutableLiveData(R.mipmap.ic_light)
    val background = MutableLiveData(R.mipmap.bg_shadow)
    val isModeON = MutableLiveData(false)
    val level = MutableLiveData(0)
    val levelText = MutableLiveData<String>()
    val maxLevel = MutableLiveData(100)
    val isShowSwitch = MutableLiveData(true)

    var currentDevice: Device? = null

    fun initData() {

    }

    fun initDevice(device: Device, viewLifecycleOwner: LifecycleOwner) {
        currentDevice = device
        name.postValue(device.name)
        when (device) {
            is Light -> {
                iconType.postValue(R.mipmap.ic_light)
                background.postValue(R.mipmap.bg_light)
                level.postValue(device.intensity)
                isModeON.postValue(device.mode == DeviceMode.ON.type)
            }
            is Heater -> {
                iconType.postValue(R.mipmap.ic_heater)
                background.postValue(R.mipmap.bg_heater)
                val maxHeaterLevel = (HEATER_MAX_TEMP - HEATER_MIN_TEMP) / HEATER_STEP
                maxLevel.postValue(maxHeaterLevel.toInt())
                level.postValue(device.temperature)
                isModeON.postValue(device.mode == DeviceMode.ON.type)
            }
            is RollerShutter -> {
                iconType.postValue(R.mipmap.ic_rollershutter)
                background.postValue(R.mipmap.bg_rollershutter)
                level.postValue(device.position)
                isShowSwitch.postValue(false)
            }
        }
        level.observe(viewLifecycleOwner) {
            if (device is Heater) {
                val newTempValue = "${HEATER_MIN_TEMP + (HEATER_STEP * it)}°C"
                levelText.postValue(newTempValue)
            } else {
                levelText.postValue("$it%")
            }
        }
    }

    val swipeOnOffListener = object : MotionLayout.TransitionListener {
        override fun onTransitionStarted(layout: MotionLayout?, p1: Int, p2: Int) {

        }

        override fun onTransitionChange(layout: MotionLayout?, p1: Int, p2: Int, p3: Float) {

        }

        override fun onTransitionCompleted(layout: MotionLayout, p1: Int) {
            isModeON.postValue(layout.currentState == R.id.end)
        }

        override fun onTransitionTrigger(layout: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {

        }
    }

    fun onBackClick(view: View) {
        App.currentActivity().onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        //Save device state when exit screen
        when (val currentDevice = currentDevice) {
            is Light -> {
                currentDevice.intensity = level.value ?: 0
                currentDevice.mode =
                    if (isModeON.value == true) DeviceMode.ON.type else DeviceMode.OFF.type
                appDatabase.insertDevice(currentDevice) {}
            }
            is Heater -> {
                currentDevice.temperature = level.value ?: 0
                currentDevice.mode =
                    if (isModeON.value == true) DeviceMode.ON.type else DeviceMode.OFF.type
                appDatabase.insertDevice(currentDevice) {}
            }
            is RollerShutter -> {
                currentDevice.position = level.value ?: 0
                appDatabase.insertDevice(currentDevice) {}
            }
        }
    }

}