package com.datpham.modulo_test.fragment.profile

import androidx.fragment.app.activityViewModels
import com.datpham.basekotlin.base.context.BaseFragment
import com.datpham.modulo_test.R
import com.datpham.modulo_test.activity.MainViewModel
import com.datpham.modulo_test.databinding.FragmentProfileBinding

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>() {

    private val shareViewModel: MainViewModel by activityViewModels()

    override fun getLayout(): Int {
        return R.layout.fragment_profile
    }

    override fun getViewModel(): Class<ProfileViewModel> {
        return ProfileViewModel::class.java
    }

    override fun initComponents() {
        mViewModel.initData()
        shareViewModel.userData.observe(viewLifecycleOwner) {
            mViewModel.initUser(it)
        }
    }

}