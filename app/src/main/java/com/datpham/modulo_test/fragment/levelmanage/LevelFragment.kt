package com.datpham.modulo_test.fragment.levelmanage

import androidx.fragment.app.activityViewModels
import com.datpham.basekotlin.base.context.BaseFragment
import com.datpham.modulo_test.R
import com.datpham.modulo_test.activity.MainViewModel
import com.datpham.modulo_test.databinding.FragmentLevelBinding

class LevelFragment : BaseFragment<FragmentLevelBinding, LevelViewModel>() {

    private val shareViewModel: MainViewModel by activityViewModels()

    override fun getLayout(): Int {
        return R.layout.fragment_level
    }

    override fun getViewModel(): Class<LevelViewModel> {
        return LevelViewModel::class.java
    }

    override fun initComponents() {
        mViewModel.initData()
        shareViewModel.deviceData.observe(viewLifecycleOwner) {
            mViewModel.initDevice(it, viewLifecycleOwner)
        }
    }

}