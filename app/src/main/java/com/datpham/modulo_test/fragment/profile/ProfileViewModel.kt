package com.datpham.modulo_test.fragment.profile

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.datpham.basekotlin.base.context.App
import com.datpham.basekotlin.base.context.BaseViewModel
import com.datpham.basekotlin.base.utils.DateTimeUtil
import com.datpham.basekotlin.base.utils.isValidDate
import com.datpham.basekotlin.base.utils.toDate
import com.datpham.basekotlin.network.entities.Address
import com.datpham.basekotlin.network.entities.User
import com.datpham.modulo_test.R
import com.datpham.modulo_test.activity.MainViewModel

class ProfileViewModel : BaseViewModel() {

    val shareViewModel = App.currentActivity().mViewModel as MainViewModel

    val avatar = MutableLiveData<String>()
    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val birthDay = MutableLiveData<String>()
    val city = MutableLiveData<String>()
    val postalCode = MutableLiveData<String>()
    val street = MutableLiveData<String>()
    val streetCode = MutableLiveData<String>()
    val country = MutableLiveData<String>()

    fun initData() {

    }

    fun initUser(user: User) {
        avatar.postValue(user.getDefaultAvatar())
        firstName.postValue(user.firstName)
        lastName.postValue(user.lastName)
        birthDay.postValue(DateTimeUtil.formatDate(user.birthDate))
        city.postValue(user.address.city)
        postalCode.postValue("${user.address.postalCode}")
        street.postValue(user.address.street)
        streetCode.postValue(user.address.streetCode)
        country.postValue(user.address.country)
    }

    fun onCloseClick(view: View) {
        App.currentActivity().onBackPressed()
    }

    fun onConfirmClick(view: View) {
        //Check null or empty
        arrayOf(
            avatar.value,
            firstName.value,
            lastName.value,
            birthDay.value,
            city.value,
            postalCode.value,
            street.value,
            streetCode.value,
            country.value
        ).forEach {
            if (it.isNullOrBlank()) {
                showToast(R.string.please_fill_all_information)
                return
            }
        }
        //Check valid birthday format
        if (!birthDay.value!!.isValidDate("dd/MM/yyyy")) {
            showToast(R.string.invalid_birthday)
            return
        }
        //All information is ready now
        val birthdayMilisecond = birthDay.value!!.toDate("dd/MM/yyyy")!!.time
        val address =
            Address(
                city.value!!,
                postalCode.value!!.toInt(),
                street.value!!,
                streetCode.value!!,
                country.value!!
            )
        val newUserInfo = User(firstName.value!!, lastName.value!!, address, birthdayMilisecond)
        shareViewModel.userData.postValue(newUserInfo)
        App.currentActivity().onBackPressed()
    }

}