package com.datpham.modulo_test.fragment.home

import androidx.fragment.app.activityViewModels
import com.datpham.basekotlin.base.context.BaseFragment
import com.datpham.basekotlin.base.utils.ConfirmDialog
import com.datpham.modulo_test.R
import com.datpham.modulo_test.activity.MainViewModel
import com.datpham.modulo_test.databinding.FragmentHomeBinding

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    private val shareViewModel: MainViewModel by activityViewModels()

    override fun getLayout(): Int {
        return R.layout.fragment_home
    }

    override fun getViewModel(): Class<HomeViewModel> {
        return HomeViewModel::class.java
    }

    override fun initComponents() {
        mViewModel.initData()
        shareViewModel.userData.observe(viewLifecycleOwner) {
            mViewModel.user.postValue(it)
        }
    }

}